title: Flutter - 跨平台开发从入门到还没放弃
speaker: njuxjy
url: https://juejin.im/user/594a8e1dac502e006bc53de1
js:
    - https://www.echartsjs.com/asset/theme/infographic.js
plugins:
    - echarts: {theme: infographic}
    - mermaid: {theme: forest}
    - katex


<slide class="bg-black-blue aligncenter" image="http://cdn.osxdaily.com/wp-content/uploads/2019/06/macOS-Catalina-Dark-Mode.jpg .dark">

# Flutter {.text-landing.text-shadow}

跨平台开发从入门到还没放弃 {.text-intro.animated.fadeInUp.delay-500}

by njuxjy {.text-intro}

[:fa-github: Github](https://github.com/flutter/flutter){.button.ghost.animated.flipInX.delay-1200}


<slide class="bg-black-blue" :class="size-50">

##  肖佳益

沪江网校 - 移动开发组打杂工程师 {.text-intro}

* :主业\::{.text-label}  iOS 技术优化 {.animated.fadeInUp}
* :副业1\::{.text-label} OCS 移动端维护 {.animated.fadeInUp.delay-400}
* :副业2\::{.text-label} React 后台页面开发 {.animated.fadeInUp.delay-800}
* :副业3\::{.text-label} 报障跟进 {.animated.fadeInUp.delay-1200}
* :副业4\::{.text-label} Flutter 调研与落地 {.animated.fadeInUp.delay-1600}
* :副业5\::{.text-label} 嘀嗒拼车司机 {.animated.fadeInUp.delay-2s}
{.description}

<slide class="bg-black-blue" :class=" size-40 aligncenter">

## 内容
---

* Flutter 介绍和原理 {.animated.fadeInUp style="font-size: 150%"}
* Flutter 布局 {.animated.fadeInUp.delay-400 style="font-size: 150%"}
* 开发体验 {.animated.fadeInUp.delay-400 style="font-size: 150%"}
* Flutter For Desktop {.animated.fadeInUp.delay-1200 style="font-size: 150%"}
* Flutter For Web {.animated.fadeInUp.delay-1600 style="font-size: 150%"}
* 背词 H5版本 vs Flutter版本{.animated.fadeInUp.delay-1800 style="font-size: 150%"}
* Demo {.animated.fadeInUp.delay-2s style="font-size: 150%"}

<slide class="fullscreen">

:::card

![WechatIMG56.png](https://upload-images.jianshu.io/upload_images/138119-21372e947a2705b9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240){..bg-black-blue}

---

## 一次编写，多端运行{..bg-black-blue}


<slide class="bg-black-blue">

:::{.content-left}

* H5 + 原生(Hybrid){style="font-size:200%"}
  * Cordova{style="font-size:70%"}
  * PhoneGap{style="font-size:70%"}
  * Ionic{style="font-size:70%"}
  * 小程序{style="font-size:70%"}
* JavaScript开发 + 原生渲染{style="font-size:200%"}
  * React Native{style="font-size:70%"}
  * Weex{style="font-size:70%"}
  * 快应用{style="font-size:70%"}
* 增强版Web App{style="font-size:200%"}
  * PWA{style="font-size:70%"}

:::

!![](https://upload-images.jianshu.io/upload_images/138119-2f0bc1cd60896bd2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 .size-50.right-bottom)


<slide class="bg-black-blue aligncenter">


:::card{..align-center}
   <img width=200/>     | Hybrid           | JS + 原生渲染   | PWA  |
:--------------------:|:------------------:|:-----------------------:|:-----------------------:|
**技术栈** |  Web  | Web  | Web |
**动态化** |  支持  | 支持，热更新在iOS上有风险  | 支持 |
**性能** |  复杂用户界面或动画有性能问题，依赖原生端一起优化性能（SpeedWeb）  | 原生渲染，性能高于H5，但渲染依赖JS和原生通信，可能造成卡顿；JIT效率不如AOT  | 复杂用户界面或动画有性能问题 |
**支持平台** |  全平台  | RN支持移动端+PC，Weex支持移动端，快应用支持支持国内部分安卓手机  | 支持ServiceWorker和Google Play Service的Android手机，以及iOS 11.3以上 |
**包体积** |  无额外体积  | RN和Weex需将引擎打包到App内，快应用无需打包引擎 | 无额外体积 |
**离线能力** |  不支持  | 支持  | 支持 |
**体验** |  一次编写，全端运行，需适配不同浏览器  | 一次编写，全端运行，控件系统会受原生UI系统限制，适配麻烦  | 一次编写，全端运行 |
**社区** | 资源丰富  |  资源丰富，但由于渲染依赖原生控件，当不同平台有更新时，社区控件可能会滞后 | 国内发展不好 |
:::


<slide class="bg-black-blue aligncenter">


:::card{..align-center}
   <img width=200/>     | Hybrid           | JS + 原生渲染   | PWA  | <span style="color:red">Flutter</span> |
:--------------------:|:------------------:|:-----------------------:|:-----------------------:|:-----------------------:|
**技术栈** |  Web  | Web  | Web | <span style="color:red">Dart</span> |
**动态化** |  支持  | 支持，热更新在iOS上有风险  | 支持 | <span style="color:red">不支持</span> |
**性能** |  复杂用户界面或动画有性能问题  | 原生渲染，性能高于H5，但渲染依赖JS和原生通信，可能造成卡顿；JIT效率不如AOT  | 复杂用户界面或动画有性能问题 |<span style="color:red">高</span> |
**支持平台** |  全平台  | RN支持移动端+PC，Weex支持移动端，快应用支持支持国内部分安卓手机  | 支持ServiceWorker和Google Play Service的Android手机，以及iOS 11.3以上 |<span style="color:red">全平台</span> |
**包体积** |  无额外体积  | RN和Weex需将引擎打包到App内，快应用无需打包引擎 | 无额外体积 |<span style="color:red">Android无需打包渲染引擎，iOS需要</span> |
**离线能力** |  不支持  | 支持  | 支持 |<span style="color:red">支持</span> |
**体验** |  一次编写，全端运行，需适配不同浏览器  | 一次编写，全端运行，控件系统会受原生UI系统限制  | 一次编写，全端运行 |<span style="color:red">一次编写，全端运行</span> |
**社区** | 资源丰富  |  资源丰富，但由于渲染依赖原生控件，当不同平台有更新时，社区控件可能会滞后 | 国内发展不好 |<span style="color:red">社区蓬勃发展+谷歌+阿里等大厂推动</span> |
:::


<slide class="bg-black-blue aligncenter">

## Flutter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vs &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;React Native

<div>

:::card{..align-center}
   <div>     |    React Native        | Flutter   |
:--------------------:|:------------------:|:-----------------------:|
**开发语言** | JavaScript   | Dart  |
**空项目打包大小** | Android 20M(可调整至 7.3M) / IOS 1.6M   | Android 5.2M / IOS 10.1M   |
**代码产物** |  JS Bundle 文件    | 二进制文件   |
**风格**      | 响应式，Learn once, write anywhere | 响应式，一次编写，多端运行  |
**支持平台**          | Android、IOS、(PC)    | Android、IOS、(Web/PC)  |
:::


<slide class="bg-black-blue">

:::{.content-left}

### Flutter

:::flexblock {.specs}
::fa-apple::

## 原生性能

渲染引擎Skia、同时支持AOT和JIT、无锁GC

---

::fa-rocket::

## 快速开发

亚秒级，有状态的热重载

---

::fa-life-ring::

## 统一的应用开发体验

多端一致的开发方式，Material Design和iOS风格

---

::fa-code::

## 响应式框架

现代，前端同学上手快
:::


!![](https://cdn.jsdelivr.net/gh/flutterchina/website@1.0/images/homepage/header-illustration.png .size-50.right-bottom)


<slide class="bg-black-blue aligncenter">

## Hot Reload

<slide class="bg-black-blue aligncenter" image="https://cdn.jsdelivr.net/gh/flutterchina/website@1.0/images/intellij/hot-reload.gif">

<slide class="bg-black-blue aligncenter" image="https://upload-images.jianshu.io/upload_images/138119-9f913ab45beea750.gif?imageMogr2/auto-orient/strip">

<slide class="bg-black-blue aligncenter" image="https://upload-images.jianshu.io/upload_images/138119-97d4e1f3a01033fb.gif?imageMogr2/auto-orient/strip">

<slide class="bg-black-blue aligncenter">

## 响应式框架

<div>

:::card{..bg-light}
```dart
class CounterState extends State<Counter> {
  int counter = 0;

  void increment() {
    // 告诉Flutter state已经改变, Flutter会调用build()，更新显示
    setState(() {
      counter++;
    });
  }

  Widget build(BuildContext context) {
    // 当 setState 被调用时，这个方法都会重新执行.
    // Flutter 对此方法做了优化，使重新执行变的很快
    // 所以你可以重新构建任何需要更新的东西，而无需分别去修改各个widget
    return new Row(
      children: <Widget>[
        new RaisedButton(
          onPressed: increment,
          child: new Text('Increment'),
        ),
        new Text('Count: $counter'),
      ],
    );
  }
}
```
:::

<slide class="bg-black-blue aligncenter">

## 多端一致性

<slide class="fullscreen">

:::card{.bg-black-blue}

![iwords_iphonexr.png](https://upload-images.jianshu.io/upload_images/138119-ec06010a9988e407.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240){style="object-fit: contain;"}

---

## iPhone XR

:::

<slide class="fullscreen">

:::card{.bg-black-blue}

![iwords_iphone8.png](https://upload-images.jianshu.io/upload_images/138119-6233378beddda23b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240){style="object-fit: contain;"}

---

## iPhone 8

:::


<slide class="fullscreen">

:::card{.bg-black-blue}

![iwords_iphone5s.png](https://upload-images.jianshu.io/upload_images/138119-51c16127cbcecdf4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240){style="object-fit: contain;"}

---

## iPhone 5s

:::


<slide class="fullscreen">

:::card{.bg-black-blue}

![iwords_ipad.png](https://upload-images.jianshu.io/upload_images/138119-ecd6401597c8260c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240){style="object-fit: contain;"}

---

## iPad Pro 9.7-inch

:::

<slide class="fullscreen">

:::card{.bg-black-blue}

![iwords_android.png](https://upload-images.jianshu.io/upload_images/138119-096620dd6c47fede.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240){style="object-fit: contain;"}

---

## Android华为 nova 3e

:::

<slide class="fullscreen">

:::card{.bg-black-blue}

![iwords_pc.png](https://upload-images.jianshu.io/upload_images/138119-2867534080f383d4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240){style="object-fit: contain;"}

---

## 桌面端

:::

<slide class="fullscreen">

:::card{.bg-black-blue}

![iwords_web.png](https://upload-images.jianshu.io/upload_images/138119-ed52665246f09939.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240){style="object-fit: contain;"}

---

## Web端

:::

<slide class="bg-black-blue" image="https://upload-images.jianshu.io/upload_images/138119-d75febbaba8812cf.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 .left-bottom">

:::div {.content-right}
> "如果用了 Flutter 以后可以节省移动端一半的开发资源，你问我资瓷不资瓷，我肯定是资瓷的。如果 Web 和桌面端也能用 Flutter 来开发，这是坠吼的。"
> ==Cindy Zhao, Senior PM of Hujiang.==
:::

<slide class="bg-black-blue aligncenter">

## Flutter架构和实现原理

<slide class="bg-black-blue aligncenter">

![2210749654-5c99897ada96d_articlex.jpeg](https://upload-images.jianshu.io/upload_images/138119-f56cdef3420cebcd.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240){style="object-fit: contain;width:100%;height:100%;"}

<slide class="bg-black-blue aligncenter">

![1261274352-5c99897a6ce37_articlex.jpeg](https://upload-images.jianshu.io/upload_images/138119-3b7ce9b934337b4d.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240){style="object-fit: contain;width:100%;height:100%;"}


<slide class="bg-black-blue aligncenter">

Widget 对象构建完成后进入渲染阶段，包括三步：{.text-intro}

* :布局元素\::{.text-label}  决定页面元素在屏幕上的位置和大小 {.animated.fadeInUp}
* :绘制阶段\::{.text-label} 将页面元素绘制成它们应有的样式 {.animated.fadeInUp.delay-400}
* :合成阶段\::{.text-label} 按照绘制规则将之前两个步骤的产物组合在一起 {.animated.fadeInUp.delay-800}

<slide class="bg-black-blue aligncenter">

![4278031179-5c99897a7164e_articlex.jpeg](https://upload-images.jianshu.io/upload_images/138119-67e544481dd5ad2b.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240){style="object-fit: contain;width:100%;height:100%;"}


<slide class="bg-black-blue aligncenter">

## Flutter布局

<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*dkZ0sQRPFhGf9r7sO4LrzA.png){style="object-fit: contain;width:100%;height:100%;"}

---
![1.png](https://miro.medium.com/max/320/1*Z8Utwfw9vPALRY0XOS4uSQ.png){style="object-fit: contain;width:100%;height:100%;"}

---

:::card{..bg-light}
```dart
Row /*or Column*/( 
  mainAxisAlignment: MainAxisAlignment.start,
  children: <Widget>[
    Icon(Icons.star, size: 50),
    Icon(Icons.star, size: 50),
    Icon(Icons.star, size: 50),
  ],
),
```
:::

:::

<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*NKci3PDfzyxSlcoZzN9WQg.png){style="object-fit: contain;width:100%;height:100%;"}

---
![1.png](https://miro.medium.com/max/320/1*SDjCqaKjWtUSwTT5Ik_-Cw.png){style="object-fit: contain;width:100%;height:100%;"}

---

:::card{..bg-light}
```dart
Row /*or Column*/( 
  mainAxisAlignment: MainAxisAlignment.center,
  children: <Widget>[
    Icon(Icons.star, size: 50),
    Icon(Icons.star, size: 50),
    Icon(Icons.star, size: 50),
  ],
),
```
:::

:::

<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*OdahXv1kvQAdn7PnsEKz7w.png){style="object-fit: contain;width:100%;height:100%;"}

---
![1.png](https://miro.medium.com/max/320/1*vpvXC7fTvKWw-w9MGADbUg.png){style="object-fit: contain;width:100%;height:100%;"}

---

:::card{..bg-light}
```dart
Row /*or Column*/( 
  mainAxisAlignment: MainAxisAlignment.spaceBetween,
  children: <Widget>[
    Icon(Icons.star, size: 50),
    Icon(Icons.star, size: 50),
    Icon(Icons.star, size: 50),
  ],
),
```
:::

:::


<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*xPKN3e4hH54TxqIwLUn42A.png){style="object-fit: contain;width:100%;height:100%;"}

---
![1.png](https://miro.medium.com/max/320/1*sAyW0aFJIYy4p1G3ekyrQQ.png){style="object-fit: contain;width:100%;height:100%;"}

---

:::card{..bg-light}
```dart
Row /*or Column*/( 
  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  children: <Widget>[
    Icon(Icons.star, size: 50),
    Icon(Icons.star, size: 50),
    Icon(Icons.star, size: 50),
  ],
),
```
:::

:::

<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*LnttjdiEBxXI_mmWrHtxmw.png){style="object-fit: contain;width:100%;height:100%;"}

---
![1.png](https://miro.medium.com/max/320/1*U38GUiD37VN0qN_ZexkIiQ.png){style="object-fit: contain;width:100%;height:100%;"}

---

:::card{..bg-light}
```dart
Row /*or Column*/( 
  mainAxisAlignment: MainAxisAlignment.spaceAround,
  children: <Widget>[
    Icon(Icons.star, size: 50),
    Icon(Icons.star, size: 50),
    Icon(Icons.star, size: 50),
  ],
),
```
:::

:::

<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*MtiSZgu4yK6A4fSpGv6Zkg.png){style="object-fit: contain;width:80%;height:80%;"}

---

:::card{..bg-light}
```dart
Row(
  crossAxisAlignment: CrossAxisAlignment.baseline,
  textBaseline: TextBaseline.alphabetic,
  children: <Widget>[
    Text(
      'Baseline',
      style: Theme.of(context).textTheme.display3,
    ),
    Text(
      'Baseline',
      style: Theme.of(context).textTheme.body1,
    ),
  ],
),
```
:::

:::

<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*uJGtSW3UO8AjMgsViSmf6A.png){style="object-fit: contain;width:100%;height:100%;"}

---
![1.png](https://miro.medium.com/max/320/1*VOB1npP6r7NNXG5gKYY3LQ.png){style="object-fit: contain;width:100%;height:100%;"}

---

:::card{..bg-light}
```dart
Row /*or Column*/( 
  crossAxisAlignment: CrossAxisAlignment.start,
  children: <Widget>[
    Icon(Icons.star, size: 50),
    Icon(Icons.star, size: 200),
    Icon(Icons.star, size: 50),
  ],
),
```
:::

:::


<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*NVCZBvvLBjcKKWU2tn7M9g.png){style="object-fit: contain;width:100%;height:100%;"}

---
![1.png](https://miro.medium.com/max/320/1*q-6779AyXXa5jTtBeQdxWQ.png){style="object-fit: contain;width:100%;height:100%;"}

---

:::card{..bg-light}
```dart
Row /*or Column*/( 
  crossAxisAlignment: CrossAxisAlignment.center,
  children: <Widget>[
    Icon(Icons.star, size: 50),
    Icon(Icons.star, size: 200),
    Icon(Icons.star, size: 50),
  ],
),
```
:::

:::

<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*Vw2RkN4cDilzbx_Jx1l1_Q.png){style="object-fit: contain;width:100%;height:100%;"}

---
![1.png](https://miro.medium.com/max/320/1*1gS9EP_Sta161SH4G_panQ.png){style="object-fit: contain;width:100%;height:100%;"}

---

:::card{..bg-light}
```dart
Row /*or Column*/( 
  crossAxisAlignment: CrossAxisAlignment.end,
  children: <Widget>[
    Icon(Icons.star, size: 50),
    Icon(Icons.star, size: 200),
    Icon(Icons.star, size: 50),
  ],
),
```
:::

:::

<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*67uV89an2F8qTEO2zEQkOA.png){style="object-fit: contain;width:100%;height:100%;"}

---
![1.png](https://miro.medium.com/max/320/1*DQgDsWne5dp8dc0ZZ911Zg.png){style="object-fit: contain;width:100%;height:100%;"}

---

:::card{..bg-light}
```dart
Row /*or Column*/( 
  crossAxisAlignment: CrossAxisAlignment.stretch,
  children: <Widget>[
    Icon(Icons.star, size: 50),
    Icon(Icons.star, size: 200),
    Icon(Icons.star, size: 50),
  ],
),
```
:::

:::

<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*8mB-TuJQHf5uz0LNrAJPNA.png){style="object-fit: contain;width:100%;height:100%;"}

---
![1.png](https://miro.medium.com/max/320/1*mgRukOgzaOutbVFWGzii-A.png){style="object-fit: contain;width:100%;height:100%;"}

---

:::card{..bg-light}
```dart
Row /*or Column*/( 
  mainAxisSize: MainAxisSize.max,
  children: <Widget>[
    Icon(Icons.star, size: 50),
    Icon(Icons.star, size: 50),
    Icon(Icons.star, size: 50),
  ],
),
```
:::

:::

<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*j6rXI3zzwHlkQHb_9FNKOw.png){style="object-fit: contain;width:100%;height:100%;"}

---
![1.png](https://miro.medium.com/max/320/1*TFkYeR-yqfHDH3pECJMM4Q.png){style="object-fit: contain;width:100%;height:100%;"}

---

:::card{..bg-light}
```dart
Row /*or Column*/( 
  mainAxisSize: MainAxisSize.min,
  children: <Widget>[
    Icon(Icons.star, size: 50),
    Icon(Icons.star, size: 50),
    Icon(Icons.star, size: 50),
  ],
),
```
:::

:::

<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*9Ap8DHjFJssXHwkMFOu5zw.png){style="object-fit: contain;width:80%;height:80%;"}

---

:::card{..bg-light}
```dart
Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(title: Text('IntrinsicWidth')),
    body: Center(
      child: Column(
        children: <Widget>[
          RaisedButton(
            onPressed: () {},
            child: Text('Short'),
          ),
          RaisedButton(
            onPressed: () {},
            child: Text('A bit Longer'),
          ),
          RaisedButton(
            onPressed: () {},
            child: Text('The Longest text button'),
          ),
        ],
      ),
    ),
  );
}
```
:::

:::


<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*JS9b6Cvb-o2FGGgIC6zPiQ.png){style="object-fit: contain;width:80%;height:80%;"}

---

:::card{..bg-light}
```dart
Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(title: Text('IntrinsicWidth')),
    body: Center(
      child: IntrinsicWidth(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            RaisedButton(
              onPressed: () {},
              child: Text('Short'),
            ),
            RaisedButton(
              onPressed: () {},
              child: Text('A bit Longer'),
            ),
            RaisedButton(
              onPressed: () {},
              child: Text('The Longest text button'),
            ),
          ],
        ),
      ),
    ),
  );
}
```
:::

:::

<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*3E_ll9conv_Ha7xTLtIn6Q.png){style="object-fit: contain;width:80%;height:80%;"}

---

:::card{..bg-light}
```dart
Widget build(BuildContext context) {
  Widget main = Scaffold(
    appBar: AppBar(title: Text('Stack')),
  );

  return Stack(
    fit: StackFit.expand,
    children: <Widget>[
      main,
      Banner(
        message: "Top Start",
        location: BannerLocation.topStart,
      ),
      Banner(
        message: "Top End",
        location: BannerLocation.topEnd,
      ),
      Banner(
        message: "Bottom Start",
        location: BannerLocation.bottomStart,
      ),
      Banner(
        message: "Bottom End",
        location: BannerLocation.bottomEnd,
      ),
    ],
  );
}
```
:::

:::


<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*CkTumWbumdO9Ka6Mwa4S2A.png){style="object-fit: contain;width:80%;height:80%;"}

---

:::card{..bg-light}
```dart
Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(title: Text('Stack')),
    body: Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Material(color: Colors.yellowAccent),
        Positioned(
          top: 0,
          left: 0,
          child: Icon(Icons.star, size: 50),
        ),
        Positioned(
          top: 340,
          left: 250,
          child: Icon(Icons.call, size: 50),
        ),
      ],
    ),
  );
}
```
:::

:::


<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*0_0q8qAbw4T_-gChblfV-A.png){style="object-fit: contain;width:80%;height:80%;"}

---

:::card{..bg-light}
```dart
Widget build(BuildContext context) {
  const iconSize = 50;
  return Scaffold(
    appBar: AppBar(title: Text('Stack with LayoutBuilder')),
    body: LayoutBuilder(
      builder: (context, constraints) =>
        Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Material(color: Colors.yellowAccent),
            Positioned(
              top: 0,
              child: Icon(Icons.star, size: iconSize),
            ),
            Positioned(
              top: constraints.maxHeight - iconSize,
              left: constraints.maxWidth - iconSize,
              child: Icon(Icons.call, size: iconSize),
            ),
          ],
        ),
    ),
  );
}
```
:::

:::


<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*7CQEUQgzAHvbmJQwnessWA.png){style="object-fit: contain;width:80%;height:80%;"}

---

:::card{..bg-light}
```dart
Row(
  children: <Widget>[
    Expanded(
      child: Container(
        decoration: const BoxDecoration(color: Colors.red),
      ),
      flex: 3,
    ),
    Expanded(
      child: Container(
        decoration: const BoxDecoration(color: Colors.green),
      ),
      flex: 2,
    ),
    Expanded(
      child: Container(
        decoration: const BoxDecoration(color: Colors.blue),
      ),
      flex: 1,
    ),
  ],
),
```
:::

:::

<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*c3l5JxXfY6-v6awf2VgggA.png){style="object-fit: contain;width:80%;height:80%;"}

---

:::card{..bg-light}
```dart
Card(child: const Text('Hello World!'), color: Colors.yellow)
```
:::

:::


<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*QCTdn09Lb5uO4ZDuCGs1LA.png){style="object-fit: contain;width:80%;height:80%;"}

---

:::card{..bg-light}
```dart
ConstrainedBox( 
  constraints: BoxConstraints.expand(),
  child: const Card(
    child: const Text('Hello World!'), 
    color: Colors.yellow,
  ), 
),
```
:::

:::


<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*q4nM3zvOd1PQFQMxCqZueQ.png){style="object-fit: contain;width:80%;height:80%;"}

---

:::card{..bg-light}
```dart
ConstrainedBox(
  constraints: BoxConstraints.expand(height: 300),
  child: const Card(
    child: const Text('Hello World!'), 
    color: Colors.yellow,
  ),
),
```
:::

:::

<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*PLsAfFDKge7Gr7yl3M_iTA.png){style="object-fit: contain;width:80%;height:80%;"}

---

:::card{..bg-light}
```dart
Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(title: Text('Container as a layout')),
    body: Container(
      color: Colors.yellowAccent,
      child: Text("Hi"),
    ),
  );
}
```
:::

:::

<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*6Q_ynFTU1rDVZ65VCJsPlQ.png){style="object-fit: contain;width:80%;height:80%;"}

---

:::card{..bg-light}
```dart
Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(title: Text('Container as a layout')),
    body: Container(
      height: double.infinity,
      width: double.infinity,
      color: Colors.yellowAccent,
      child: Text("Hi"),
    ),
  );
}
```
:::

:::

<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*Zc3fvnsiRq_P_8luY2_BCQ.png){style="object-fit: contain;width:80%;height:80%;"}

---

:::card{..bg-light}
```dart
SizedBox.expand(
  child: Card(
    child: Text('Hello World!'),
    color: Colors.yellowAccent,
  ),
),
```
:::

:::

<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*UuPNTwfn0_U-PnczL0rSqg.png){style="object-fit: contain;width:80%;height:80%;"}

---

:::card{..bg-light}
```dart
Column(
  children: <Widget>[
    Icon(Icons.star, size: 50),
    const SizedBox(height: 100),
    Icon(Icons.star, size: 50),
    Icon(Icons.star, size: 50),
  ],
),
```
:::

:::


<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*80OncmeIsFh_T__VRTtO5A.png){style="object-fit: contain;width:100%;height:100%;"}

---
![1.png](https://miro.medium.com/max/320/1*l6RFIsNdaX5p5Xj_HbZ0Iw.png){style="object-fit: contain;width:100%;height:100%;"}

---

:::card{..bg-light}
```dart
Widget build(BuildContext context) {
  bool isVisible = ...
  return Scaffold(
    appBar: AppBar(
      title: Text('isVisible = $isVisible'),
    ),
    body: isVisible 
      ? Icon(Icons.star, size: 150) 
      : const SizedBox(),
  );
}
```
:::

:::

<slide class="bg-black-blue aligncenter">

:::column {.vertical-align}

![1.png](https://miro.medium.com/max/320/1*zaPTNnqSOLy4zj-usj-3Mw.png){style="object-fit: contain;width:100%;height:100%;"}

---
![1.png](https://miro.medium.com/max/320/1*nbp1xowGxriVW9aNQn_20A.png){style="object-fit: contain;width:100%;height:100%;"}

---

:::card{..bg-light}
```dart
Widget build(BuildContext context) {
  return Material(
    color: Colors.blue,
    child: SafeArea(
      child: SizedBox.expand(
        child: Card(color: Colors.yellowAccent),
      ),
    ),
  );
}
```
:::

:::


<slide class="bg-black-blue aligncenter">

## 开发体验

* 热重载
* 同时调试多设备
* Dart Devtools
* VSCode
  * 自动拉取依赖包
  * 缓解嵌套地狱
  * 重构工具

<slide class="bg-black-blue aligncenter">

## Flutter For Desktop

<slide class="bg-black-blue" :class="size-30 aligncenter">

### Install

---

`go get -u github.com/go-flutter-desktop/hover` {.animated.fadeInUp}

`hover init myAwesomeApp` {.animated.delay-400.fadeInUp}

`hover run` {.animated.delay-800.fadeInUp}

<slide class="bg-black-blue aligncenter">

## Flutter For Web

<slide class="bg-black-blue aligncenter">

## 目前处于Preview，坑很多

可以参考 [我的踩坑经历](https://juejin.im/post/5d4be5d25188251f6b1ef68f)

<slide class="bg-black-blue aligncenter">

## 背词 H5版本 vs Flutter版本

<slide class="bg-black-blue aligncenter">

:::column

![iwords_h5.gif](https://upload-images.jianshu.io/upload_images/138119-f86ba5014e45d4bb.gif?imageMogr2/auto-orient/strip){style="object-fit: contain;width:60%;height:60%;"}

---

![iwords_flutter.gif](https://upload-images.jianshu.io/upload_images/138119-1568e2420575d1ee.gif?imageMogr2/auto-orient/strip){style="object-fit: contain;width:60%;height:60%;"}

:::

<slide class="bg-black-blue aligncenter">

:::column

![iwords_h5_landscape.gif](https://upload-images.jianshu.io/upload_images/138119-5ed6e7dc1286572e.gif?imageMogr2/auto-orient/strip){style="object-fit: contain;width:100%;height:100%;"}

---

![iwords_flutter_landscape.gif](https://upload-images.jianshu.io/upload_images/138119-0a4960b640e80817.gif?imageMogr2/auto-orient/strip){style="object-fit: contain;width:100%;height:100%;"}

:::

<slide class="bg-black-blue aligncenter">

## 对Flutter的期待

* 动态化，热更新 ([MXFlutter](https://github.com/TGIF-iMatrix/MXFlutter))
* 对桌面端和Web端有更好的支持

<slide class="bg-black-blue aligncenter" image="http://cdn.osxdaily.com/wp-content/uploads/2019/06/macOS-Catalina-Dark-Mode.jpg .dark.anim">

## Write code, blow mind {.animated.tada}

快使用 [Flutter](https://github.com/flutter/flutter) 写一个 **hello world** 吧 {.text-into.animated.delay-800.fadeIn}

有兴趣也可以参加我们的Flutter项目，欢迎PR\: {.text-into.animated.delay-800.fadeIn}

[:fa-cloud-download: iWords](https://gitlab.yeshj.com/area51/iwords){.button.animated.delay-1s.fadeInUp}
